﻿Read and agree with ALL terms of use written in this txt file before using this MikuMikuDance Model Data. If you do not agree with all terms of use, or if you do not understand any part of this terms of use txt file, you are NOT allowed to use this MikuMikuDance Model Data.
-------

This MMD Model data may only be used for non-profit purposes. 
Do not use this MMD Model data for commercial purposes.

Do not re-distribute this MMD Model Data.

Private Trading and Public Trading of this MMD Model Data is NOT allowed.

Although you are allowed to edit this MMD Model Data, you may NOT distribute that MMD Model Data to other users.

Do not carry out 3D printing using this data. 

Do not use this MMD Model in ANY sexual, erotic relationships.

Do not use this MMD Model for works which include YAOI/YURI content. (In this case, "YAOI/YURI content" includes but is not limited to Shounen-ai (Shonen-ai), Shoujo-ai (Shojo-ai), BL (Boys' love), GL (Girls' Love), Slash, Femmeslash (Femslash), homosexual, homoerotic, and homoromantic relationships.)


Please use this MMD Model Data at your own risk. I can not be held liable or responsible for any damages caused by using this data.

This MMD Model Data may not be used for any works which contain negative content toward the original product, individual, &/or group. 

-------
If you want to edit this model, please observe the rule written in readme_Japanese.txt .
If you can't understand the readme_Japanese.txt, you must not edit this model. 
-------

MikuMikuDance Model created by NASU.


